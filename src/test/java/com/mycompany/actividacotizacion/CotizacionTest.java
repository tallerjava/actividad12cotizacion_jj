package com.mycompany.actividacotizacion;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionTest {

    @Test
    public void getFecha_objetoCotizacionConValoresNormales_obtenerFecha() {
        Date resultadoEsperado = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(resultadoEsperado, moneda, precio);
        Date resultadoObtenido = instance.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void getPrecio_objetoCotizacionConValoresNormales_obtenerPrecio() {
        Date fecha = new Date(118, 8, 8);
        double resultadoEsperado = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(fecha, moneda, resultadoEsperado);
        double resultadoObtenido = instance.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.009);
    }

    @Test
    public void setFecha_objetoCotizacionConValoresNormales_cambiarFecha() {
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(fecha, moneda, precio);
        Date resultadoEsperado = new Date(118, 8, 9);
        instance.setFecha(resultadoEsperado);
        Date resultadoObtenido = instance.getFecha();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void setPrecio_objetoCotizacionConValoresNormales_cambiarPrecio() {
        Date fecha = new Date(118, 8, 8);
        double precio = 12.123f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(fecha, moneda, precio);
        double resultadoEsperado = 1554.1115;
        instance.setPrecio(resultadoEsperado);
        double resultadoObtenido = instance.getPrecio();
        assertEquals(resultadoEsperado, resultadoObtenido, 0.009);
    }

    @Test
    public void toString_objetoCotizacionConValoresNormales_obtenerString() {
        Date fecha = new Date(118, 8, 8, 11, 19);
        double precio = 4212.129f;
        String moneda = "USD";
        Cotizacion instance = new Cotizacion(fecha, moneda, precio);
        String resultadoEsperado = "8-9-2018 11:19 / USD 4212,13";
        String resultadoObtenido = instance.toString();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

}
