package com.mycompany.actividacotizacion;

import java.io.IOException;
import java.text.ParseException;
import org.apache.http.conn.HttpHostConnectException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CotizacionServiceTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() throws IOException, ParseException {
        CotizacionService instance = new CotizacionService(new CoinDeskCotizacionRepository());
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpHostConnectException.class)
    public void obtenerCotizacion_servicioNoResponde_HttpHostConnectException() throws IOException, ParseException {
        CoinDeskCotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        repositorio.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        CotizacionService instance = new CotizacionService(repositorio);
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}
