package com.mycompany.actividacotizacion;

import java.io.IOException;
import java.text.ParseException;
import org.apache.http.conn.HttpHostConnectException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoinDeskCotizacionRepositoryTest {

    @Test
    public void obtenerCotizacion_servicioResponde_objetoCotizacionNotNull() throws IOException, ParseException {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
        assertNotNull(resultadoObtenido);
    }

    @Test(expected = HttpHostConnectException.class)
    public void obtenerCotizacion_servicioNoResponde_HttpHostConnectException() throws IOException, ParseException {
        CoinDeskCotizacionRepository instance = new CoinDeskCotizacionRepository();
        instance.setUrl("https://api.coindes.com/v1/bpi/currentprice.json");
        Cotizacion resultadoObtenido = instance.obtenerCotizacion();
    }

}
