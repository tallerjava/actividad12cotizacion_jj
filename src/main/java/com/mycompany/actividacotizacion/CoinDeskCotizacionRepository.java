package com.mycompany.actividacotizacion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.JSONObject;
import org.apache.http.impl.client.HttpClientBuilder;

public class CoinDeskCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";

    public String getUrl(String url) {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Cotizacion obtenerCotizacion() throws IOException, ParseException {
        Cotizacion cotizacion;
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        HttpEntity entity = response.getEntity();
        String linea;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()))) {
            linea = reader.readLine();
        }
        JSONObject namecampo = new JSONObject(linea);
        String moneda = namecampo.getJSONObject("bpi").getJSONObject("USD").getString("code");
        double cotizacionBitcoin = namecampo.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM d, yyyy hh:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date fecha = simpleDateFormat.parse(namecampo.getJSONObject("time").getString("updated"));
        cotizacion = new Cotizacion(fecha, moneda, cotizacionBitcoin);
        return cotizacion;
    }
}
