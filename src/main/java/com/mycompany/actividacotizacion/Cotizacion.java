package com.mycompany.actividacotizacion;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cotizacion {

    private Date fecha;
    private double precio;
    private String moneda;

    public Cotizacion(Date fecha, String moneda, double precio) {
        this.fecha = fecha;
        this.precio = precio;
        this.moneda = moneda;
    }

    public Date getFecha() {
        return fecha;
    }

    public double getPrecio() {
        return precio;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        DecimalFormat decimalFormat = new DecimalFormat("####.##");
        SimpleDateFormat formateador = new SimpleDateFormat("d-M-yyyy hh:mm");
        return formateador.format(fecha) + " / " + moneda + " " + decimalFormat.format(this.precio);
    }
}
