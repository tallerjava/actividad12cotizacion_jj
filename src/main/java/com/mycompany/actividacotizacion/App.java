package com.mycompany.actividacotizacion;

import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws IOException, ParseException {
        CotizacionRepository repositorio = new CoinDeskCotizacionRepository();
        CotizacionService cotizacionService = new CotizacionService(repositorio);
        try (Scanner sc = new Scanner(System.in)) {
            System.out.println("Presionar ENTER para ver la cotización y para salir culquier otra tecla y ENTER");
            String enterkey = sc.nextLine();
            while (enterkey.isEmpty()) {
                System.out.println(cotizacionService.obtenerCotizacion());
                System.out.println("(Presione Enter para volver a consultar)");
                enterkey = sc.nextLine();
            }
        }
        System.out.println("Salió de la Cotización");
    }
}
