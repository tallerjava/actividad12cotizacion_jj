package com.mycompany.actividacotizacion;

import java.io.IOException;
import java.text.ParseException;

public abstract class CotizacionRepository {

    public abstract Cotizacion obtenerCotizacion() throws IOException, ParseException;
}
