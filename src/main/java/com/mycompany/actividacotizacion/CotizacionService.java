package com.mycompany.actividacotizacion;

import java.io.IOException;
import java.text.ParseException;

public class CotizacionService {

    private final CotizacionRepository repositorio;

    public CotizacionService(CotizacionRepository repositorio) {
        this.repositorio = repositorio;
    }

    public Cotizacion obtenerCotizacion() throws IOException, ParseException {
        return repositorio.obtenerCotizacion();
    }
}
